<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory {

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
       
        return [
            'user_id' =>1,
            'title' => 'My Amazing Product',
            'price' => 100,
            'description' => 'This product will change the way you wash your dishes forever'
        ];
        
    }

}
