<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\User;


class HomeController extends Controller {

    //
    public function index(Request $request) {
        $title = "welcome";
        $products = DB::table('products')->first();

        return view('home.index',['products'=>$products]);
    }

}
